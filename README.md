# Learning Networking (Unity-Style)  
This code is a GitLab fork from Tom Weiland's [TCP-UDP-Networking](https://github.com/tom-weiland/tcp-udp-networking) repo on GitHub.  
It is derived from the core code from his [C# networking tutorial series](https://www.youtube.com/playlist?list=PLXkn83W0QkfnqsK8I0RAz5AbUxfg3bOQ5) on YouTube.