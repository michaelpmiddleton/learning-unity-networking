﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class ClientHandle : MonoBehaviour {
    public static void Welcome(Packet packet) {
        string msg = packet.ReadString();
        int myId = packet.ReadInt();

        Debug.Log($"Message from server: {msg}");
        Client.instance.myId = myId;
        ClientSend.WelcomeReceived();

        // Now that we have the client's id, connect UDP
        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void PlayerSpawn(Packet packet) {
        int id = packet.ReadInt();
        string username = packet.ReadString();
        Vector3 position = packet.ReadVector3();

        Debug.Log($"Spawning player {username}({id}) at {position}");

        // Spawn either a player or THE player based on ID
        Player newPlayer = Instantiate(id == Client.instance.myId ? 
            GameManager.instance.localPlayerPrefab :
            GameManager.instance.playerPrefab
            ).GetComponent<Player>();

        GameManager.players.Add(id, newPlayer);

        // Put new player in proper position
        newPlayer.transform.position = position;    
    }

    public static void PlayerPosition(Packet packet) {
        int id = packet.ReadInt();
        Vector3 pos = packet.ReadVector3();

        GameManager.players[id].transform.position = pos;
    }

    public static void PlayerDisconnected(Packet packet) { }

    public static void PlayerScored(Packet packet) { }

    public static void PuckPosition(Packet packet) { }

    public static void PuckSpawned(Packet packet) { }

    public static void PuckDespawned(Packet packet) { }
}
