﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour {
    // Based on Tom Weiland's C# Networking Library -- DO NOT TOUCH
    #region Packet Transfer Protocols
    /// <summary>Sends a packet to the server via TCP.</summary>
    /// <param name="_packet">The packet to send to the sever.</param>
    private static void SendTCPData(Packet _packet) {
        _packet.WriteLength();
        Client.instance.tcp.SendData(_packet);
    }

    /// <summary>Sends a packet to the server via UDP.</summary>
    /// <param name="_packet">The packet to send to the sever.</param>
    private static void SendUDPData(Packet _packet) {
        _packet.WriteLength();
        Client.instance.udp.SendData(_packet);
    }
    #endregion

    // PONG-specific packet functions
    #region Packet Definitions
    /// <summary>Lets the server know that the welcome message was received.</summary>
    public static void WelcomeReceived() {
        using (Packet packet = new Packet((int)ClientPackets.welcomeReceived)) {
            packet.Write(Client.instance.myId);
            packet.Write("PLACEHOLDER FOR USERNAME RESOURCE");

            SendTCPData(packet);
        }
    }

    /// <summary>Sends player input to the server.</summary>
    /// <param name="inputs">2D Array that contains WASD/Arrow input</param>
    public static void PlayerMovement(float[] inputs) {
        using (Packet packet = new Packet((int)ClientPackets.playerMovement)) {
            // Define length of input array, then define array
            packet.Write(2);
            foreach (float input in inputs)
                packet.Write(input);
            
            SendUDPData(packet);
        }
    }
    #endregion
}
