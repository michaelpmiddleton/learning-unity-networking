﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public int id;
    public string username;
    public MeshRenderer model;

    public void Initialize(int id, string username) {
        this.id = id;
        this.username = username;
    }
}
