﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPlayer : MonoBehaviour {
    public Transform camTransform;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            // ClientSend.PlayerShoot(camTransform.forward);
            // PAUSE
            Debug.Log($"Pause requested by Player");
        }
    }

    private void FixedUpdate() {
        SendInputToServer();
    }

    /// <summary>Sends player input to the server.</summary>
    private void SendInputToServer() {
        float[] inputs = {
            Input.GetKey(KeyCode.A) ? -1 : Input.GetKey(KeyCode.D) ? 1 : 0,
            Input.GetKey(KeyCode.W) ? 1 : Input.GetKey(KeyCode.S) ? -1 : 0,
        };

        ClientSend.PlayerMovement(inputs);
    }
}
