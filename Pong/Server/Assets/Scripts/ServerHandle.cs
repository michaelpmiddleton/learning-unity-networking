﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle {
    public static void WelcomeReceived(int sourceClient, Packet packet) {
        int _clientIdCheck = packet.ReadInt();
        string _username = packet.ReadString();

        Debug.Log($"{Server.clients[sourceClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {sourceClient}.");
        if (sourceClient != _clientIdCheck) {
            Debug.Log($"Player \"{_username}\" (ID: {sourceClient}) has assumed the wrong client ID ({_clientIdCheck})!");
        }
        Server.clients[sourceClient].SendIntoGame(_username);
    }

    public static void PlayerMovement(int sourceClient, Packet packet) {
        float[] inputs = new float[packet.ReadInt()];
        inputs[0] = packet.ReadFloat();
        inputs[1] = packet.ReadFloat();

        Server.clients[sourceClient].player.SetInput(inputs);
    }

    public static void PlayerPause(int sourceClient, Packet packet) {
        // TODO: Handle pausing here.
        // packet has a bool for pause state, call ServerSend.GameStateChange
    }
}
