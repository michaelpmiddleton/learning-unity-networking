﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerSend {
    // Based on Tom Weiland's C# Networking Library -- DO NOT TOUCH
    #region Packet Transfer Protocols
    /// <summary>Sends a packet to a client via TCP.</summary>
    /// <param name="recipient">The client to send the packet the packet to.</param>
    /// <param name="packet">The packet to send to the client.</param>
    private static void SendTCPData(int recipient, Packet packet) {
        packet.WriteLength();
        Server.clients[recipient].tcp.SendData(packet);
    }

    /// <summary>Sends a packet to a client via UDP.</summary>
    /// <param name="recipient">The client to send the packet the packet to.</param>
    /// <param name="packet">The packet to send to the client.</param>
    private static void SendUDPData(int recipient, Packet packet) {
        packet.WriteLength();
        Server.clients[recipient].udp.SendData(packet);
    }

    /// <summary>Sends a packet to all clients via TCP.</summary>
    /// <param name="packet">The packet to send.</param>
    private static void SendTCPDataToAll(Packet packet) {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++) {
            Server.clients[i].tcp.SendData(packet);
        }
    }
    /// <summary>Sends a packet to all clients except one via TCP.</summary>
    /// <param name="exceptClient">The client to NOT send the data to.</param>
    /// <param name="packet">The packet to send.</param>
    private static void SendTCPDataToAll(int exceptClient, Packet packet) {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++) {
            if (i != exceptClient) {
                Server.clients[i].tcp.SendData(packet);
            }
        }
    }

    /// <summary>Sends a packet to all clients via UDP.</summary>
    /// <param name="packet">The packet to send.</param>
    private static void SendUDPDataToAll(Packet packet) {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++) {
            Server.clients[i].udp.SendData(packet);
        }
    }
    /// <summary>Sends a packet to all clients except one via UDP.</summary>
    /// <param name="exceptClient">The client to NOT send the data to.</param>
    /// <param name="packet">The packet to send.</param>
    private static void SendUDPDataToAll(int exceptClient, Packet packet) {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++) {
            if (i != exceptClient) {
                Server.clients[i].udp.SendData(packet);
            }
        }
    }
    #endregion

    // PONG-specific packet functions
    #region Packet Definitions
    /// <summary>Sends a welcome message to the given client.</summary>
    /// <param name="recipient">The client to send the packet to.</param>
    /// <param name="msg">The message to send.</param>
    public static void Welcome(int recipient, string msg) {
        using (Packet packet = new Packet((int)ServerPackets.welcome)) {
            packet.Write(msg);
            packet.Write(recipient);

            SendTCPData(recipient, packet);
        }
    }

    /// <summary>Tells a client to spawn a player.</summary>
    /// <param name="recipient">The client that should spawn the player.</param>
    /// <param name="player">The player to spawn.</param>
    public static void SpawnPlayer(int recipient, Player player) {
        using (Packet packet = new Packet((int)ServerPackets.playerSpawn)) {
            packet.Write(player.id);
            packet.Write(player.username);
            packet.Write(player.transform.position);

            SendTCPData(recipient, packet);
        }
    }

    /// <summary>Tells clients to spawn the puck. </summary>
    public static void SpawnPuck(int recipient, Vector3 position) {
        using(Packet packet = new Packet((int)ServerPackets.puckSpawned)) {
            packet.Write(position);
            SendTCPData(recipient, packet);
        }
    }

    /// <summary>Sends a player's updated position to all clients.</summary>
    /// <param name="player">The player whose position to update.</param>
    public static void PlayerPosition(Player player) {
        using (Packet packet = new Packet((int)ServerPackets.playerPosition)) {
            packet.Write(player.id);
            packet.Write(player.transform.position);

            SendUDPDataToAll(packet);
        }
    }

    public static void PuckPosition(Vector3 position) {
        using(Packet packet = new Packet((int)ServerPackets.puckPosition)) {
            packet.Write(position);
            SendUDPDataToAll(packet);
        }
    }

    public static void PlayerDisconnected(int playerId) {
        using (Packet packet = new Packet((int)ServerPackets.playerDisconnected)) {
            packet.Write(playerId);

            SendTCPDataToAll(packet);
        }
    }
    #endregion
}
