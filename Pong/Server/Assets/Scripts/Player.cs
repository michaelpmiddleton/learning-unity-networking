﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public int id;
    public string username;
    public float moveSpeed = 5f;
    public Rigidbody rb;

    private float[] inputs = {0f, 0f};


    public void Initialize(int id, string username) {
        this.id = id;
        this.username = username;
    }



    /// <summary>Processes player input and moves the player.</summary>
    public void FixedUpdate() {
        Vector3 inputDirection = Vector3.zero;
        inputDirection.x = inputs[0];
        inputDirection.y = inputs[1];
        Move(inputDirection);
    }



    /// <summary>Calculates the player's desired movement direction and moves him.</summary>
    /// <param name="inputDirection">Vector3 containting WASD/Arrow key input</param>
    private void Move(Vector3 inputDirection) {
        if (inputDirection.magnitude > 0)
            rb.AddForce((inputDirection * moveSpeed).normalized, ForceMode.Impulse);
        else
            rb.velocity = Vector3.zero;

        ServerSend.PlayerPosition(this);
    }



    /// <summary>Updates the player input with newly received input.</summary>
    /// <param name="inputs">The new key inputs</param>
    public void SetInput(float[] inputs) {
        this.inputs = inputs;
    }
}
