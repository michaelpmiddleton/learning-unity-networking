﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puck : MonoBehaviour {
    public static Puck instance = null;

    /// <summary>Send position of puck to players</summary>
    public void FixedUpdate() {
        // Send where I am to the players
        ServerSend.PuckPosition(gameObject.transform.position);
    }
}
