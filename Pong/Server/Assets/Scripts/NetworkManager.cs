﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour {
    public static NetworkManager instance;

    public GameObject playerPrefab;
    public GameObject puckPrefab;
    public Vector3[] playerSpawnPosition;


    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != this) {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start() {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        Server.Start(2, 26950);
    }

    private void OnApplicationQuit() {
        Server.Stop();
    }

    public Player InstantiatePlayer(bool isLeft) {
        return Instantiate(playerPrefab, playerSpawnPosition[isLeft ? 0 : 1], Quaternion.identity).GetComponent<Player>();
    }

    public Puck InstantiatePuck() {
        return Instantiate(puckPrefab, new Vector3(-3f, 6f, 0f), Quaternion.identity).GetComponent<Puck>();
    }
}
