﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    public int id;
    public string username;
    public float health;
    public float maxHealth = 100f;
    public int itemCount = 0;
    public bool colorIsA = true;
    public MeshRenderer model;

    public void Initialize(int _id, string _username) {
        id = _id;
        username = _username;
        health = maxHealth;
    }

    public void SetHealth(float _health) {
        health = _health;

        if (health <= 0f) {
            Die();
        }
    }

    public void ChangeColor(bool toggle) {
        // Set colorIsA to the server's data
        colorIsA = toggle;

        // Update local graphics
        model.material = colorIsA ? GameManager.instance.playerMaterialA : GameManager.instance.playerMaterialB;
    }

    public void Die() {
        model.enabled = false;
    }

    public void Respawn() {
        model.enabled = true;
        SetHealth(maxHealth);
    }
}
